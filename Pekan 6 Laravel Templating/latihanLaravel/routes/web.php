<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\KomentarController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'index']);
Route::get('/register', [AuthController::class, 'regist']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/data-tables', [IndexController::class, 'table']);

Route::middleware(['auth'])->group(function () {
// CRUD Genre

// Create
// route yang mengarah ke form tambah Genre

Route::get("/genre/create",[GenreController::class, 'create']);

// route tambah data ke database
Route::post('/genre',[GenreController::class, 'store']);

// Read
// route untuk menampilkan semua data yang ada di database table Genre
Route::get('/genre',[GenreController::class, 'index']);
// Route detail Genre berdasarkan id
Route::get('/genre/{id}',[GenreController::class, 'show']);

// Update
// Route untuk mengarah ke form update Genre berdasarkan id
Route::get('/genre/{id}/edit',[GenreController::class, 'edit']);

// Route untuk update data berdasarkan id di table Genre database
Route::put('/genre/{id}',[GenreController::class, 'update']);

// Delete
// Route hapus data di tabel Genre database berdasarkan id
Route::delete('/genre/{id}',[GenreController::class, 'destroy']);

Route::post('/komentar',[KomentarController::class, 'store']);

});

// CRUD Cast

// Create
// route yang mengarah ke form tambah cast

Route::get("/cast/create",[CastController::class, 'create']);

// route tambah data ke database
Route::post('/cast',[CastController::class, 'store']);

// Read
// route untuk menampilkan semua data yang ada di database table kategori
Route::get('/cast',[CastController::class, 'index']);
// Route detail cast berdasarkan id
Route::get('/cast/{id}',[CastController::class, 'show']);

// Update
// Route untuk mengarah ke form update cast berdasarkan id
Route::get('/cast/{id}/edit',[CastController::class, 'edit']);
// Route untuk update data berdasarkan id di table cast database
Route::put('/cast/{id}',[CastController::class, 'update']);

// Delete
// Route hapus data di tabel cast database berdasarkan id
Route::delete('/cast/{id}',[CastController::class, 'destroy']);


Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


// CRUD Film

// Create
// route yang mengarah ke form tambah Film

Route::get("/film/create",[FilmController::class, 'create']);

// route tambah data ke database
Route::post('/film',[FilmController::class, 'store']);

// Read
// route untuk menampilkan semua data yang ada di database table film
Route::get('/film',[FilmController::class, 'index']);
// Route detail film berdasarkan id
Route::get('/film/{id}',[FilmController::class, 'show']);

// Update
// Route untuk mengarah ke form update film berdasarkan id
Route::get('/film/{id}/edit',[FilmController::class, 'edit']);
// Route untuk update data berdasarkan id di table film database
Route::put('/film/{id}',[FilmController::class, 'update']);

// Delete
// Route hapus data di tabel film database berdasarkan id
Route::delete('/film/{id}',[FilmController::class, 'destroy']);


// Route::resource('film', 'FilmController');