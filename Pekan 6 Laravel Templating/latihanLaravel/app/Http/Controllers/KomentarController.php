<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Komentar;
use Illuminate\Support\Facades\Auth;

class KomentarController extends Controller
{
    public function store(Request $request)
    {
        $users_id = Auth::id();       
         $request->validate([
            'content' => 'required',
            'point' => 'required',
        ]);

        $komentar = new Komentar;
 
        $komentar->content = $request->content;
        $komentar->point = $request->point;
        $komentar->film_id = $request->film_id;
        $komentar->users_id = $users_id;

        $komentar->save();

        return redirect ('/komentar'.$request->film_id);

    }
}
