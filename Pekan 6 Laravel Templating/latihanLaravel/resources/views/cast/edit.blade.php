@extends('layout.master')

@section('title')
    Halaman Update Cast
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @method('put')
    @csrf
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="integer" name= "umur" value="{{$cast->umur}}" class="form-control"> 
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
    </div>   
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

        {{-- <input type="integer" name= "umur"class="form-control"> --}}

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection