@extends('layout.master')

@section('title')
    Halaman List Cast
@endsection

@section('content')

    <a href="/cast/create" class="btn btn-primary btn-sn my-3">Tambah</a>

    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($data as $key => $item)
            <tr>
            <th scope="row">{{$key+1}}</th>
                <td>{{$item->nama}}</td>
                <td>
                    
                    <form action="/cast/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit"class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">

                </td>
                
              </tr>
            @empty
            <tr>               
                <td>Data Cast Kosong</td>
            </tr>       
            @endforelse
         
          </tbody>
      </table>

@endsection