@extends('layout.master')

@section('title')
    Halaman Biodata
@endsection

@section('content')

    <form action="welcome" method="post">
        @csrf
        <label>First name:</label><br>
        <input type="text"name="first name"><br><br>
        <label>Last name:</label><br>
        <input type="text"name="last name"><br><br>
        <label>Gender:</label><br>
        <input type="radio"name="Gender">Male<br>
        <input type="radio"name="Gender">Female<br><br>
        <label>Nationality:</label><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Australian">Australian</option>
            <option value="Russian">Russian</option>
            <option value="Other">Other</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox"name="language">Bahasa Indonesia<br>
        <input type="checkbox"name="language">English<br>
        <input type="checkbox"name="language">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit"value="Sign Up" /> 
    </form>
@endsection