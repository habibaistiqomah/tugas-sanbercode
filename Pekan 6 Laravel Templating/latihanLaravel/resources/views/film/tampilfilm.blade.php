@extends('layout.master')

@section('title')
    Halaman List Film
@endsection

@section('content')

@auth
    <a href="/film/create" class="btn btn-primary btn-sn my-3">Tambah Film</a>
@endauth    
    <div class="row">
        @forelse ($data as $item)
            <div class="col-4">
                <div class="card">
                    <img src="{{asset('images/'.$item->poster)}}" class="card-img-top" width="" height="400px"  alt="...">
                    <div class="card-body">
                        <span class="badge text-bg-warning">{{$item->genre->name}}</span>
                        <h3>{{$item->judul}}</h3>
                        <p class="card-text">{{ Str::limit($item->ringkasan, 60) }}</p>
                        <a href="/film/{{$item->id}}" class="btn btn-primary btn-block btn-sm">Read More</a>
            
                        <div class="row my-2"> 
                        <div class="col">
                            <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-block btn-sm">Edit</a>
                        </div>

                <div class="col">
                    @auth
                    <form action="film/{{$item->id}}" method ="POST">
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">

                    </form>  
                    @endauth 

                    @guest
                    <a href="/film/{{$item->id}}" class="btn btn-primary btn-block btn-sm">Read More</a>
                    @endguest
                </div>
                </div>
            </div>
          </div>
        </div>
        @empty
           <h2> Tidak ada data </h2> 

        @endforelse



@endsection