@extends('layout.master')

@section('title')
    Halaman Tambah Film
@endsection

@section('content')

<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul Film</label>
      <input type="text" name="judul" class="form-control">
    </div>
    @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Ringkasan</label>
        <textarea name="ringkasan" cols="30" rows="10" class="form-control"></textarea>
    </div>   
    @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Tahun</label>
        <input type="integer" name= "tahun"class="form-control"> 
      </div>
      @error('tahun')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Poster</label>
        <input type="file" name="poster" class="form-control">
      </div>
      @error('poster')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Genre</label>
        <select name="genre_id" class="form-control" id="">
            <option value="">-- Pilih Genre --</option>           
            @forelse ($genre as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @empty
                <option value="">Tidak ada data</option>
            @endforelse


        </select>
      </div>
      @error('genre_id')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

        {{-- <input type="integer" name= "umur"class="form-control"> --}}

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection