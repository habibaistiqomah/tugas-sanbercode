@extends('layout.master')

@section('title')
    Detail Film
@endsection

@section('content')
<div class="row">
    <div class="card">
    <div class="card-body">     
    <h3>{{$film->judul}}</h3><br>
    <img src="{{asset('images/'.$film->poster)}}" class="img-fluid" width="400" alt="..."><br><br>

     
    <p class="card-text">Ringkasan:</p>
    <p class="card-text">{{$film->ringkasan }}</p>

    <p class="card-text">Tahun: {{$film->tahun }}</p>

    <p class="card-text">Genre: {{$film->genre_id }}</p>

    <hr>
    <h3>List Komentar</h3>
    @forelse($film->komentar as $item)
    <h1 src="..." class="mr-3 text-info" alt="...">{{$item->point}}</h1>
    <div class="media-body">
        <h5 class="mt-0">{{$item->user->name}}</h5>   
        <p>{{$item->content}}</p>
    </div>
    </div>
    @empty
    <h6>Tidak ada komentar</h6>
    @endforelse
    <hr>
    @auth
    <form action="/komentar" method="post">
        @csrf
        <input type="hidden" value="{{$film->id}}" name="film_id">
        <div class="form-group">
            <select name="point" id="" class="form-control">
                <option value="">--Pilih Point--</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
        </div>
        @error('point')
        <div class="form-group">
            <textarea name="content" id="" class="form-control" cols="30" rows="10" placeholder="Isi Komentar"></textarea>
          </div>
          @error('content')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          <input type="submit" class="btn btn-primary btn-sm" value="Kirim Komentar">
    </form>

    <hr>
    <a href="/film" class="btn btn-primary btn-block btn-sm">Kembali</a>

</div>
</div>
</div>
@endsection