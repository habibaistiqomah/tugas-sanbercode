@extends('layout.master')

@section('title')
    Halaman List Genre
@endsection

@section('content')

    <a href="/genre/create" class="btn btn-primary btn-sn my-3">Tambah</a>

    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($data as $key => $item)
            <tr>
            <th scope="row">{{$key+1}}</th>
                <td>{{$item->name}}</td>
                <td>
                    
                    <form action="/genre/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                        <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/genre/{{$item->id}}/edit"class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form> 
                </td>
                
              </tr>
            @empty
            <tr>               
                <td>Data Genre Kosong</td>
            </tr>       
            @endforelse
         
          </tbody>
      </table>

@endsection