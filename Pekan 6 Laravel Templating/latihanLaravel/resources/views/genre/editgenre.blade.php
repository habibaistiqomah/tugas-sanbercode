@extends('layout.master')

@section('title')
    Halaman Update Genre
@endsection

@section('content')

<form action="/genre/{{$genre->id}}" method="POST">
    @method('put')
    @csrf
    <div class="form-group">
      <label>Nama Genre</label>
      <input type="text" name="name" value="{{$genre->name}}" class="form-control">
    </div>
    @error('name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
        {{-- <input type="integer" name= "umur"class="form-control"> --}}

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection