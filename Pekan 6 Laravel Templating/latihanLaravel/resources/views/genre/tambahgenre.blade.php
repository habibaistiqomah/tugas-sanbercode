@extends('layout.master')

@section('title')
    Halaman Tambah Genre
@endsection

@section('content')

<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Genre</label>
      <input type="text" name="name" class="form-control">
    </div>
    @error('name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  

        {{-- <input type="integer" name= "umur"class="form-control"> --}}

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection