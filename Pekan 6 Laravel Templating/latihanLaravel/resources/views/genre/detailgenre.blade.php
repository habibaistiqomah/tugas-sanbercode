@extends('layout.master')

@section('title')
    Detail Genre
@endsection

@section('content')

<h1>{{$genre->name}}</h1>

<div class="row">

@forelse ($genre->film as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('images/'.$item->poster)}}" class="card-img-top" style="height:400px" alt="...">
            <div class="card-body">
              <h5 class="card-title">{{$item->judul}}</h5>
              <p class="card-text">{{ Str::limit($item->ringkasan, 60)}}</p>
              <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
            </div>
          </div>
    </div>
@empty
<h3>Tidak ada film di genre ini</h3> 
@endforelse

@endsection